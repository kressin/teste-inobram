// requestJoke: retorna a quantidade de piadas de Chuck Norris
//             desejada no formato JSON.
//              Quando a requisição retorna do servidor,
//              adiciona ao ListModel passado a função.
// parâmetros: numberOfJokes (int)   = quantidade de piadas a serem retornadas
//             nameModel (ListModel) = modelo passado a lista de piadas do tipo ListView
function requestJoke(nameModel, numberOfJokes) {

    var number = 1;
    if(numberOfJokes) {
        number = numberOfJokes;
    }
    var url = "http://api.icndb.com/jokes/random/" + number;

    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", url, true);

    //Função chamada quando requisição retorna do servidor
    xhttp.onreadystatechange = function(){
        if ( xhttp.readyState == 4 && xhttp.status == 200 ) {//Verifica retorno do servidor
            var jokesData = JSON.parse(xhttp.responseText);
            var success = jokesData["type"];
            if(success) {
                writeToListView(nameModel, jokesData);
            }
        }
    }
    xhttp.send();
}

function writeToListView(nameModel, jokesData){
    nameModel.clear();
    var jokesList = jokesData["value"];
    for (var i in jokesList) {
        nameModel.append({jokeText: jokesList[i]["joke"]});
    }
}
