import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

//Modelo de cada elemento da lista
Rectangle {
    id: rectangleDelegateId
    color: lightGrayColor
    height: listViewTextId.implicitHeight*2
    width: listOfJokesId.width
    radius: 5
    border.width: 1
    border.color: darkBlueColor

    Text {
        id: listViewTextId
        anchors { left: parent.left; right: parent.right }
        textFormat: Text.StyledText
        wrapMode: Text.Wrap
        width: parent.width
        horizontalAlignment: Text.AlignHCenter
        anchors.centerIn: parent
        text: jokeText;
        font.pixelSize: 18
    }
}
