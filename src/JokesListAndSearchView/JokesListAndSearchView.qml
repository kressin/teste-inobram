import QtQuick 2.14
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

import "JokeReceiver.js" as JokeReceiver

//Página com busca e lista de piadas
Item {
    id: jokesListAndSearchViewId
    property int numberOfJokes: 3

    function hideJokesListAndSearchView() {
        stackViewId.pop()
    }

    Rectangle {
        id: backgroundJokesListAndSearchViewId
        color: ligthBlueColor
        anchors.fill: parent

        //ListView - Lista de piadas
        ListView {
            id: listOfJokesId
            width: parent.width
            height: parent.height
            anchors { left: parent.left; right: parent.right }
            anchors.top: parent.top
            anchors.bottom: numberOfJokesSpinBoxId.top
            clip: true
            model: ListModel { id: nameModel }
            delegate: listElementsModelComponentId

            Component {
                id: listElementsModelComponentId
                ListElementsModel { }
            }
        }

        //SpinBox - seleciona a quantidade de piadas
        SpinBox {
            id: numberOfJokesSpinBoxId
            value: 3
            width: parent.width
            anchors.bottom: searchButtonId.top
            onValueChanged: {
                numberOfJokes = value
            }
        }

        //Botão buscar - dispara requisição http/get com a quantidade de piadas
        //              desejadas
        Button {
            id: searchButtonId
            text: "Buscar"
            width: parent.width
            anchors.bottom: removeLastItemButtonId.top
            palette {
                button: darkGrayColor
                buttonText: "white"
            }
            onClicked: {
                JokeReceiver.requestJoke(nameModel, numberOfJokes)
            }
        }

        //Botão Remover Último - Remove último item da lista
        Button {
            id: removeLastItemButtonId
            text: "Remover Último"
            width: parent.width
            anchors.bottom: backButtonId.top
            onClicked: {
                listOfJokesId.model.remove(listOfJokesId.model.count-1);
            }
            palette {
                button: lightGrayColor
                buttonText: "black"
            }
        }

        //Botão Voltar - Remove JokesListAndSearchView da pilha de stackView
        Button {
            id: backButtonId
            text: "Voltar"
            onClicked: hideJokesListAndSearchView()
            width: parent.width
            anchors.bottom: parent.bottom
            palette {
                button: darkGrayColor
                buttonText: "white"
            }

        }
    }

}
