import QtQuick 2.14
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

//Página inicial  - botão entrar
Item {

    function showJokesListAndSearchView() {
        stackViewId.push(jokesListAndSearchViewComponentId)
    }

    Rectangle {
        color: ligthBlueColor
        anchors.fill: parent

        Button {
            anchors.centerIn: parent
            text: "Entrar"
            onClicked: showJokesListAndSearchView()
        }
    }
}
